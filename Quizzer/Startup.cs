﻿using Microsoft.Owin;
using Owin;
using Quizzer;

[assembly: OwinStartup(typeof(Startup))]
namespace Quizzer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
