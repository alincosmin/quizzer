﻿using System.Web.Mvc;

namespace Quizzer.Models
{
    public class AnswerModel
    {
        public bool IsCorrect { get; set; }
        public string Text { get; set; }
    }
}