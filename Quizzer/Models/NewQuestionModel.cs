﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Quizzer.Models
{
    public class NewQuestionModel
    {
        public int SubdomainId { get; set; }

        [Display(Name = "Domain")]
        public string DomainName { get; set; }

        [Display(Name = "Type")]
        public bool IsManyChoice { get; set; }

        public int CorrectAnswerId { get; set; }

        public string Text { get; set; }

        public List<AnswerModel> Answers { get; set; } 
    }
}