﻿using System;
using DataManager;

namespace Quizzer.Models
{
    public class QuizModel
    {
        public string Id { get; set; }
        public double Score { get; set; }
        public int DomainId { get; set; }
        public int Status { get; set; }
        public string Date { get; set; }
        public bool ReadOnly { get; set; }

        public QuizModel(QuizSet quiz)
        {
            Id = quiz.Id.ToString();
            DomainId = quiz.DomainId;
            Status = quiz.Status;
            Score = quiz.Score;
            Date = quiz.Date.ToShortDateString();
            ReadOnly = quiz.DateLimit < DateTime.Now || quiz.Status > 1;
        }
    }
}