﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataManager;

namespace Quizzer.Models
{
    public class NewSubdomainModel
    {
        [Display(Name = "Domain")]
        public int DomainId { get; set; }

        [Display(Name = "Domain name")]
        public string DomainName { get; set; }

        [Display(Name = "Domain description")]
        public string DomainDescription { get; set; }
        
        [Display(Name = "Subdomain name")]
        public string SubdomainName { get; set; }

        [Display(Name = "Subdomain description")]
        public string SubdomainDescription { get; set; }
        public SelectList Domains { get; set; }
    }
}