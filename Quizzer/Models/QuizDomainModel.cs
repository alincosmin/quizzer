﻿using System.Collections.Generic;
using DataManager;

namespace Quizzer.Models
{
    public class QuizDomainModel
    {
        public List<Domain> Domains { get; set; }
        public List<QuizModel> Quizes { get; set; } 
    }
}