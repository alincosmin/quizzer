﻿using System;
using System.Collections.Generic;
using DataManager;

namespace Quizzer.Models
{
    public class QuizQuestionModel
    {
        public List<QuestionModel> Questions { get; set; }
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
        public bool IsWithMultipleChoice { get; set; }
        public string SelectedAnswer { get; set; }
        public List<QuizAnswerModel> Answers { get; set; }
        public string QuizSetId { get; set; }
        public int Page { get; set; }
        public bool IsAnswered { get; set; }
        public string DomainName { get; set; }
        public bool ReadOnly { get; set; }
        public DateTime DateLimit { get; set; }
    }

    public class QuestionModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public bool IsAnswered { get; set; }
    }

    public class QuizAnswerModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public bool IsSelected { get; set; }
    }
}