﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataManager;
using Microsoft.AspNet.Identity;
using Quizzer.Models;
using WebGrease.Css.Extensions;

namespace Quizzer.Controllers
{
    [Authorize(Roles = "Student")]
    public class QuizController : Controller
    {
        private DomainService _domainService = new DomainService();
        private QuizService _quizService = new QuizService();
        private Random _random = new Random(DateTime.Now.Millisecond);

        public ActionResult Index()
        {
            var domains = _domainService.GetDomains().ToList();
            var quizes = _quizService.GetForUser(User.Identity.GetUserId()).ToList().Select(x => new QuizModel(x)).ToList();

            return View(new QuizDomainModel()
            {
                Domains = domains,
                Quizes = quizes
            });
        }

        public ActionResult GetQuiz(int domainId)
        {
            var quizSets = _quizService.GetForUser(User.Identity.GetUserId()).Where(x => x.DomainId == domainId && x.Status == 1 && DateTime.Now < x.DateLimit).ToList();
            if (quizSets.Any())
            {
                return RedirectToAction("TakeQuiz", new { id = quizSets.First().Id.ToString()});
            }

            var simpleCount = 3 + _random.Next(2);
            var domain = _domainService.GetDomains().First(x => x.Id == domainId);

            var simpleQuestions =
                domain.SubDomains.SelectMany(s => s.QuizItems)
                    .Where(q => !q.HasManyAnswers)
                    .OrderBy(x => _random.NextDouble())
                    .Take(simpleCount).ToList();

            var multipleQuestions = domain.SubDomains.SelectMany(s => s.QuizItems)
                    .Where(q => q.HasManyAnswers)
                    .OrderBy(x => _random.NextDouble())
                    .Take(10 - simpleCount).ToList();

            var questions = new List<QuizItem>(simpleQuestions);
            questions.AddRange(multipleQuestions);
            questions = questions.OrderBy(x => _random.NextDouble()).ToList();

            var quiz = _quizService.GenerateQuiz(User.Identity.GetUserId(), questions, domainId);
            var newQuiz = _quizService.GetForUser(User.Identity.GetUserId()).First(x => x.Id == quiz.Id);

            return RedirectToAction("TakeQuiz", new { id = newQuiz.Id.ToString()});
        }
        
        [RestoreModelStateFromTempData]
        public ActionResult TakeQuiz(string id, int page = 0)
        {
            var guid = Guid.Parse(id);
            var quiz =
                _quizService.GetForUser(User.Identity.GetUserId())
                    .First(x => x.Id == guid);
            var questions = quiz.QuizSetQuestions.Select(x => new QuestionModel()
            {
                Id = x.QuizItem.Id.ToString(),
                Text = x.QuizItem.Description,
                IsAnswered = quiz.QuizSetAnswers.Any(y => y.QuizItemId == x.QuizItemId)
            }).ToList();

            var model = new QuizQuestionModel();
            model.Questions = questions;

            model.QuizSetId = quiz.Id.ToString();
            var domain = _domainService.GetDomains().First(x => x.Id == quiz.DomainId);
            model.DomainName = domain != null ? domain.Name : "";

            model.ReadOnly = quiz.Status > 1 || quiz.DateLimit < DateTime.Now;
            model.DateLimit = quiz.DateLimit;

            if (page >= 1 && page <= 10)
            {
                var question = quiz.QuizSetQuestions.ElementAt(page - 1);

                model.QuestionId = question.QuizItemId.ToString();
                model.QuestionText = question.QuizItem.Description;
                model.IsWithMultipleChoice = question.QuizItem.HasManyAnswers;
                model.Page = page;
                model.QuizSetId = quiz.Id.ToString();

                var answers = question.QuizItem.Answers.Select(x => new QuizAnswerModel()
                {
                    Id = x.Id.ToString(),
                    Text = x.Description,
                    IsSelected = quiz.QuizSetAnswers.Any(y => y.AnswerId == x.Id)
                }).ToList();

                model.IsAnswered = answers.Any(x => x.IsSelected);
                model.Answers = answers;

                if (!model.IsWithMultipleChoice && model.Answers.Any(x => x.IsSelected))
                    model.SelectedAnswer = model.Answers.First(x => x.IsSelected).Id;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SetTempDataModelState]
        public ActionResult EndQuiz(string id)
        {
            var guid = Guid.Parse(id);
            ComputeScore(guid);

            return RedirectToAction("TakeQuiz", new { id = guid.ToString()});
        }

        private void ComputeScore(Guid guid)
        {
            var quiz =
                _quizService.GetForUser(User.Identity.GetUserId())
                    .First(x => x.Id == guid);

            var score = 0.0;
            foreach (var question in quiz.QuizSetAnswers)
            {
                var questionScore = 0.0;

                var givenAnswers =
                    quiz.QuizSetAnswers.Where(x => x.QuizItemId == question.QuizItemId).Select(x => x.AnswerId).ToList();
                var correctAnswers = question.QuizItem.Answers.Where(x => x.IsCorrect).Select(x => x.Id).ToList();
                foreach (var answerId in givenAnswers)
                {
                    questionScore += correctAnswers.Any(x => x.Equals(answerId)) ? 1 : -1;
                }

                if (questionScore > 0)
                    score += questionScore/correctAnswers.Count;
            }

            _quizService.UpdateQuizScore(quiz, score);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SetTempDataModelState]
        public ActionResult SaveQuizAnswer(QuizQuestionModel model)
        {
            var guid = Guid.Parse(model.QuizSetId);
            var quizItem = _quizService.GetForUser(User.Identity.GetUserId()).First(x => x.Id == guid);

            if (quizItem.Status > 1)
            {
                ModelState.AddModelError("expired", "You can no longer change your answer");
            }

            if (quizItem.DateLimit < DateTime.Now)
            {
                ModelState.AddModelError("timeExceeded", "You have exceeded the time limit");
                ComputeScore(guid);
            }

            if (!ModelState.IsValid)
            {
                return RedirectToAction("TakeQuiz", new { id = model.QuizSetId, page = model.Page });
            }

            if (!model.IsWithMultipleChoice)
            {
                if (string.IsNullOrWhiteSpace(model.SelectedAnswer))
                {
                    ModelState.AddModelError("noAnswerSelected", "You must select an answer");
                }
                else
                {
                    var answer = new QuizSetAnswer()
                    {
                        AnswerId = Guid.Parse(model.SelectedAnswer),
                        QuizItemId = Guid.Parse(model.QuestionId),
                        QuizSetId = quizItem.Id
                    };

                    _quizService.SaveAnswer(answer);
                }
            }
            else
            {
                if (!model.Answers.Any(x => x.IsSelected))
                {
                    ModelState.AddModelError("noAnswerSelected", "You must select an answer");
                }
                else
                {
                    var answers = model.Answers.Where(x => x.IsSelected).Select(x => new QuizSetAnswer()
                    {
                        AnswerId = Guid.Parse(x.Id),
                        QuizItemId = Guid.Parse(model.QuestionId),
                        QuizSetId = quizItem.Id
                    }).ToList();

                    _quizService.SaveAnswers(answers);
                }
            }

            return RedirectToAction("TakeQuiz", new {id = model.QuizSetId, page = model.Page});
        }
	}
}