﻿using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Quizzer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var id = User.Identity.GetUserId();
            return View();
        }

    }
}