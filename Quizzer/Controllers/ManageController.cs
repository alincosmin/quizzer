﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataManager;
using Quizzer.Models;

namespace Quizzer.Controllers
{
    [Authorize(Roles = "Professor")]
    public class ManageController : Controller
    {
        private DomainService _domainService = new DomainService();
        private QuestionService _questionService = new QuestionService();

        public ActionResult Index()
        {
            return RedirectToAction("Domains");
        }

        public ActionResult Domains()
        {
            var domains = _domainService.GetDomains().ToList();
            return View(domains);
        }

        public ActionResult Questions(int subDomainId)
        {
            var subdomain = _domainService.GetSubDomains().FirstOrDefault(x => x.Id == subDomainId);
            if (subdomain != null)
            {
                var questions = subdomain.QuizItems;
                ViewData["subdomainId"] = subDomainId; 
                return View(questions);
            }
            else return RedirectToAction("Domains");
        }

        public ActionResult AddSubdomain()
        {
            //return RedirectToAction("Domains");
            var domains = _domainService.GetDomains().ToList();
            domains.Add(new Domain()
            {
                Id = -1,
                Name = "- Add new domain -"
            });

            var list = new SelectList(domains, "Id", "Name");
            var model = new NewSubdomainModel()
            {
                Domains = list
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult AddSubdomain(NewSubdomainModel model)
        {
            Domain domain = null;
            if (model.DomainId == -1)
            {
                domain = _domainService.CreateDomain(model.DomainName, model.DomainDescription);
            }
            else
            {
                domain = _domainService.GetDomains().FirstOrDefault(x => x.Id == model.DomainId);
            }

            var subDomain = new SubDomain()
            {
                DomainId = domain.Id,
                Name = model.SubdomainName,
                Description = model.SubdomainDescription
            };

            _domainService.CreateSubdomain(subDomain);

            return RedirectToAction("Domains");
        }

        public ActionResult AddQuestion(int subdomainId)
        {
            var subdomain = _domainService.GetSubDomains().First(x => x.Id == subdomainId);

            var model = new NewQuestionModel()
            {
                DomainName = string.Format("{0}: {1}", subdomain.Domain.Name, subdomain.Name)
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult AddQuestion(NewQuestionModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Text))
            {
                ModelState.AddModelError("incompleteQuestion", "The question is incomplete");
            }

            var answers = model.Answers.Where(x => !string.IsNullOrWhiteSpace(x.Text)).ToList();
            if (!model.IsManyChoice && model.CorrectAnswerId >= 0 && answers.Count() > model.CorrectAnswerId)
                answers[model.CorrectAnswerId].IsCorrect = true;

            if (answers.Count() < 3)
            {
                ModelState.AddModelError("incompleteAnswers", "You have incomplete answers");
            }

            if (!answers.Any(x => x.IsCorrect))
            {
                ModelState.AddModelError("correctNotSelected", "You must select at least a correct answer");
            }

            var question = new QuizItem()
            {
                HasManyAnswers = model.IsManyChoice,
                SubDomainId = model.SubdomainId,
                Description = model.Text
            };

            var newAnswers = answers.Select(x => new Answer()
            {
                IsCorrect = x.IsCorrect,
                Description = x.Text,
            }).ToList();

            if (ModelState.IsValid)
            {

                question.Answers = newAnswers;
                _questionService.Save(question);
                return RedirectToAction("Questions", new {subDomainId = model.SubdomainId});
            }

            return View(model);
        }
    }
}
