﻿using System;
using System.Linq;

namespace DataManager
{
    public static class UserUtils
    {
        public static IQueryable<QuizSet> GetQuizez(string userID)
        {
            return
                new QuizzerEntities().QuizSets.Where(x => x.UserId.Equals(userID, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}