﻿using System.Linq;

namespace DataManager
{
    public class QuestionService
    {
        public void Save(QuizItem question)
        {
            using (var ctx = new QuizzerEntities())
            {
                ctx.QuizItems.Add(question);
                ctx.SaveChanges();
            }
        }
    }
}