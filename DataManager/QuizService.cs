﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace DataManager
{
    public class QuizService
    {
        public IQueryable<QuizSet> GetForUser(string userId)
        {
            return
                new QuizzerEntities().QuizSets.Where(
                    x => x.UserId.Equals(userId, StringComparison.InvariantCultureIgnoreCase));
        }

        public QuizSet GenerateQuiz(string userId, IList<QuizItem> questions, int domainId = -1)
        {
            QuizSet quiz;
            using (var ctx = new QuizzerEntities())
            {
                quiz = new QuizSet()
                {
                    UserId = userId,
                    Status = 1,
                    DomainId = domainId
                };
                ctx.QuizSets.Add(quiz);

                var quizQuestions = questions.Select(x => new QuizSetQuestion() {QuizItemId = x.Id, QuizSet = quiz});
                ctx.QuizSetQuestions.AddRange(quizQuestions);

                ctx.SaveChanges();
            }

            return quiz;
        }

        public void SaveAnswer(QuizSetAnswer answer)
        {
            using (var ctx = new QuizzerEntities())
            {
                var quiz = ctx.QuizSets.First(x => x.Id == answer.QuizSetId);
                var existing = quiz.QuizSetAnswers.Where(x => x.QuizItemId == answer.QuizItemId);
                if (existing.Any())
                    ctx.QuizSetAnswers.RemoveRange(existing);

                ctx.QuizSetAnswers.Add(answer);
                ctx.SaveChanges();
            }
        }

        public void SaveAnswers(List<QuizSetAnswer> answers)
        {
            using (var ctx = new QuizzerEntities())
            {
                var guid = answers.First().QuizSetId;
                var quiz = ctx.QuizSets.First(x => x.Id == guid);
                var ids = answers.Select(x => x.QuizItemId).ToArray();
                var existing = quiz.QuizSetAnswers.Where(x => ids.Contains(x.QuizItemId));
                if (existing.Any())
                    ctx.QuizSetAnswers.RemoveRange(existing);

                ctx.QuizSetAnswers.AddRange(answers);
                ctx.SaveChanges();
            }
        }

        public void UpdateQuizScore(QuizSet quiz, double score)
        {
            using (var ctx = new QuizzerEntities())
            {
                var dbQuiz = ctx.QuizSets.First(x => x.Id == quiz.Id);
                dbQuiz.Score = score;
                dbQuiz.Status = 2;
                ctx.SaveChanges();
            }
        }
    }
}