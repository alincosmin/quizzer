//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class Answer
    {
        public Answer()
        {
            this.QuizSetAnswers = new HashSet<QuizSetAnswer>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public System.Guid Id { get; set; }
        public string Description { get; set; }
        public System.Guid QuizItemId { get; set; }
        public bool IsCorrect { get; set; }
    
        public virtual QuizItem QuizItem { get; set; }
        public virtual ICollection<QuizSetAnswer> QuizSetAnswers { get; set; }
    }
}
