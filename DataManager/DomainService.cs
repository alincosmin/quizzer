﻿using System.Collections.Generic;
using System.Linq;

namespace DataManager
{
    public class DomainService
    {
        public IQueryable<Domain> GetDomains()
        {
            return new QuizzerEntities().Domains.AsQueryable();
        }

        public IQueryable<SubDomain> GetSubDomains()
        {
            return new QuizzerEntities().SubDomains.AsQueryable();
        }

        public Domain CreateDomain(string name, string description)
        {
            var domain = new Domain()
            {
                Name = name,
                Description = description
            };

            using (var context = new QuizzerEntities())
            {
                context.Domains.Add(domain);
                context.SaveChanges();
            }

            return domain;
        }

        public SubDomain CreateSubdomain(SubDomain subdomain)
        {
            using (var context = new QuizzerEntities())
            {
                context.SubDomains.Add(subdomain);
                context.SaveChanges();
            }

            return subdomain;
        }
    }
}